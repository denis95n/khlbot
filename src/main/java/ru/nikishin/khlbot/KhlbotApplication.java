package ru.nikishin.khlbot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KhlbotApplication {

	public static void main(String[] args) {
		SpringApplication.run(KhlbotApplication.class, args);
	}

}
